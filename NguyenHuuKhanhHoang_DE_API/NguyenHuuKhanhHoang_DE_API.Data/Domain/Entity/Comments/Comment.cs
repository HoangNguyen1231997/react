﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Comments
{
    [Table("Comment")]
    public class Comment
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string NoiDung { get; set; }
        public long NewsId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        [ForeignKey("NewsId")]
        public virtual Data.Domain.Entity.News.News News { get; set; }

    }
}
