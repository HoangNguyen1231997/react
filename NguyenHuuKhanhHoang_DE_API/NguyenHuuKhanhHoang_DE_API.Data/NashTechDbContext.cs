﻿using Microsoft.EntityFrameworkCore;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.News;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Data
{
    public class NashTechDbContext : DbContext
    {
        public NashTechDbContext(DbContextOptions<NashTechDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<NewsDetail> NewsDetails { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
