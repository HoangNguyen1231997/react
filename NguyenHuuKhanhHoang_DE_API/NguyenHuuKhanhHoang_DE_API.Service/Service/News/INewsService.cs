﻿using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Comments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.News
{
    public interface INewsService
    {
        void UpdateNews(Data.Domain.Entity.News.News newsEntity);
        Data.Domain.Entity.News.News GetNewsById(long id);
        void AddComment(Comment commentEntity);
        Task<List<Data.Domain.Entity.News.News>> GetNewList(int typeNew);
        Task<Data.Domain.Entity.News.News> GetNewsDetailValue(long id);
        void AddNewNews(Data.Domain.Entity.News.News newsEntity);
        void DeleteNews(long id);
        Task<List<Data.Domain.Entity.News.News>> GetNewListByUser(long userId);
    }
}
