﻿using Microsoft.EntityFrameworkCore;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Users;
using NguyenHuuKhanhHoang_DE_API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.Users
{
    public class UserService : IUserService
    {
        IRepository<User> _usersRepository;
        IRepository<UserRole> _userRolesRepository;
        public UserService(IRepository<User> usersRepository, IRepository<UserRole> userRolesRepository)
        {
            _usersRepository = usersRepository;
            _userRolesRepository = userRolesRepository;
        }

       
        public User GetDeatilUserLogin1(string name, string pass)
        {

            var result = _usersRepository.TableNoTracking.Include(x => x.UserRoles).ThenInclude(x => x.Role).Where(x => x.Name.Equals(name) && x.Password.Equals(pass)).FirstOrDefault();

            return result;
        }
        public async Task<bool> CheckPassWord(string pass, string conFirmPass)
        {
            if(pass!=null && conFirmPass != null)
            {
                if (conFirmPass.Equals(pass))
                {
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> IsUserExist(string userName)
        {
            var result = CheckUserExist(userName);
            if (result != null)
            {
                return false;
            }
            return true;
        }
        public User GetUserInformation(string name, string pass)
        {
            var result = _usersRepository.TableNoTracking.Include(x => x.UserRoles).ThenInclude(x => x.Role).Where(x => x.Name.Equals(name)).FirstOrDefault();
            if (result == null)
            {
                return result;
            }
            var check = CheckPassWordHash(result.Password, pass);
            if (check == false)
            {
                return null;
            }
            return result;
        }
        public async Task<User> GetUserDetail(long id)
        {
            var result = await _usersRepository.TableNoTracking.Include(x=>x.News).ThenInclude(x=>x.NewsDetail).Include(x => x.UserRoles).ThenInclude(x => x.Role).Where(x => x.Id.Equals(id)).FirstOrDefaultAsync();
           
            return result;
        }
        private bool CheckPassWordHash(string passHash, string password)
        {
            /* Fetch the stored value */
            string savedPasswordHash = passHash;
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }
        public User CheckUserExist(string name)
        {

            var result = _usersRepository.TableNoTracking.Where(x => x.Name.Equals(name)).FirstOrDefault();
            return result;
        }
        public string PasswordHash(string password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }
        public void AddNewUser(User userEntity)
        {
            _usersRepository.Add(userEntity);
        }
        public void AddNewRole(UserRole userRoleEntity)
        {
            _userRolesRepository.Add(userRoleEntity);
        }
    }
}
