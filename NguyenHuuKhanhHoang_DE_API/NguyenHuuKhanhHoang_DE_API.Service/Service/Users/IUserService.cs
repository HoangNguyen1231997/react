﻿using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.Users
{
    public interface IUserService
    {
        Task<User> GetUserDetail(long id);
        Task<bool> IsUserExist(string userName);
        Task<bool> CheckPassWord(string pass, string conFirmPass);
        User GetUserInformation(string name, string pass);
        User CheckUserExist(string name);
        void AddNewUser(User userEntity);
        void AddNewRole(UserRole userRoleEntity);
        string PasswordHash(string password);
    }
}
