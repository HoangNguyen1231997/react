﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using NguyenHuuKhanhHoang_DE_API.Auth;
using NguyenHuuKhanhHoang_DE_API.JWTToken;
using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel;
using System.Net;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Users;
using Microsoft.AspNetCore.Cors;
using NguyenHuuKhanhHoang_DE_API.Model.ErrorModel;
using NguyenHuuKhanhHoang_DE_API.Service.Service.News;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel.Validators;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Users;

namespace NguyenHuuKhanhHoang_DE_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {
       
        private IJwtFactory _jwtFactory;
        private IHttpContextAccessor _accessor;
        private IUserService _userService;
        private INewsService _newsService;
        public LoginController(IUserService userService, IHttpContextAccessor accessor, IJwtFactory jwtFactory, INewsService newsService)
        {
            _newsService = newsService;
             _accessor = accessor;
            _jwtFactory = jwtFactory;
            _userService = userService;
        }
        
        [HttpPost("Login")]
        public ActionResult Login(LoginModel loginModel)
        {
            var user = _userService.GetUserInformation(loginModel.UserName, loginModel.Password);
            if(user != null)
            {
               var role = user.UserRoles.FirstOrDefault().Role.RoleName;
                var accessUser = new AccessUser()
                {
                    AccessToken = _jwtFactory.GenerateTokenJWT(user.Id,user.Name, role),
                    UserName = user.Name,
                    Id = user.Id
                };
                return Ok(accessUser);
            }
            else
            {
                throw new ApiException("Tài khoản hoặc mật khẩu bạn nhập sai");
            }
          
        }
        [HttpPost("Register")]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            string passwordHashed = _userService.PasswordHash(registerViewModel.Password);
            var user = new User()
            {
                Name = registerViewModel.UserName,
                Password = passwordHashed,
                GioiThieu = registerViewModel.GioiThieu
            };
            _userService.AddNewUser(user);
            var getUser = _userService.CheckUserExist(registerViewModel.UserName);
            var role = new UserRole()
            {
                UserId = getUser.Id,
                RoleId = 2
            };
            _userService.AddNewRole(role);
            return Ok();
        }
       
    }
}