﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DE_API.Data.Domain.Entity.News;
using NguyenHuuKhanhHoang_DE_API.Model.CommentViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel;
using NguyenHuuKhanhHoang_DE_API.Service.Service.News;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Users;

namespace NguyenHuuKhanhHoang_DE_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private INewsService _newsService;
        private IUserService _userService;
        public HomeController(INewsService newsService, IUserService userService)
        {

            _newsService = newsService;
            _userService = userService;
        }
       
        [HttpGet("GetHeaderNews")]
        public async Task<ActionResult> GetHeaderNews()
        {
            var result = await _newsService.GetNewList(1);
            return Ok(result);
        }
        [HttpGet("GetBodyNews")]
        public async Task<ActionResult> GetBodyNews()
        {
            var result = await _newsService.GetNewList(2);
            return Ok(result);
        }
        [HttpGet("CheckToken")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> CheckToken()
        {
           
            return Ok();
        }
        [HttpGet("CheckPermission")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,Roles ="Admin")]
        public async Task<ActionResult> CheckPermission()
        {

            return Ok();
        }
        [HttpGet("GetDetailNews")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> GetDetailNews(long id)
        {
            var result = await _newsService.GetNewsDetailValue(id);
            return Ok(result);
        }
        [HttpGet("GetDetailUser")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> GetDetailUser(long id)
        {
            var result = await _userService.GetUserDetail(id);
            return Ok(result);
        }
        [HttpPost("UploadImage")]
        public ActionResult UploadImage()
        {
            string imageName = null;
            var httpRequset = HttpContext.Request.Form.Files["Image"];
            imageName = Guid.NewGuid().ToString() + "_" + httpRequset.FileName;
            string uploadFolder = "Resources/" + imageName;
            httpRequset.CopyToAsync(new FileStream(uploadFolder, FileMode.Create));
            var result = "https://localhost:5001/" + uploadFolder;
            return Ok(new { Value = result });
        }
        [HttpPost("DangBaiViet")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,Roles ="Admin")]
        public ActionResult DangBaiViet(UpLoadNewsViewModel uploadViewModel)
        {
            var newsEntity = new News()
            {
                TieuDe = uploadViewModel.TieuDe,
                TheLoai = uploadViewModel.TheLoai,
                Type = 2,
                UserId = Convert.ToInt64(uploadViewModel.UserId),
                NewsDetail = new NewsDetail()
                {
                    ImageUrl = uploadViewModel.ImageUrl,
                    ImageUrlSub = uploadViewModel.ImageUrlSub,
                    ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                    NoiDung = uploadViewModel.NoiDung,
                    NoiDungSub = uploadViewModel.NoiDungSub,
                }
            };
            _newsService.AddNewNews(newsEntity);
            return Ok();
        }
        [HttpPost("UpdateBaiViet")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult UpdateBaiViet(UpDateNewsViewModel upDateViewModel)
        {
            var newsEntity = new News()
            {
                Id = Convert.ToInt64(upDateViewModel.Id),
                TieuDe = upDateViewModel.TieuDe,
                TheLoai = upDateViewModel.TheLoai,
                Type = Convert.ToInt32(upDateViewModel.Type),
                UserId = Convert.ToInt64(upDateViewModel.UserId),
                NewsDetail = new NewsDetail()
                {
                    Id = Convert.ToInt64(upDateViewModel.NewDetailId),
                    NewsId = Convert.ToInt64(upDateViewModel.Id),
                    ImageUrl = upDateViewModel.ImageUrl,
                    ImageUrlSub = upDateViewModel.ImageUrlSub,
                    ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                    NoiDung = upDateViewModel.NoiDung,
                    NoiDungSub = upDateViewModel.NoiDungSub,
                }
            };
            _newsService.UpdateNews(newsEntity);
            return Ok();
        }
        [HttpPost("DeleteNew")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult DeleteNew(long id)
        {
            _newsService.DeleteNews(id);
            return Ok();
        }
        [HttpGet("GetDetailNewForUpdate")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult GetDetailNewForUpdate(long id)
        {
            var result = _newsService.GetNewsById(id);
            var newsUpdate = new UpDateNewsViewModel()
            {
                Id = result.Id,
                NewDetailId = result.NewsDetail.Id,
                NoiDung = result.NewsDetail.NoiDung,
                NoiDungSub = result.NewsDetail.NoiDungSub,
                ThoiGianDang = result.NewsDetail.ThoiGianDang,
                TheLoai = result.TheLoai,
                TieuDe = result.TieuDe,
                UserId = result.UserId,
                Type = result.Type,
                ImageUrl = result.NewsDetail.ImageUrl,
                ImageUrlSub = result.NewsDetail.ImageUrlSub
            };
            return Ok(newsUpdate);
        }
        [HttpPost("Comment")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult Comment(CommentViewModel commentViewModel)
        {
            var comment = new Comment() {
                NewsId=Convert.ToInt64(commentViewModel.NewsId),
                UserId =Convert.ToInt64(commentViewModel.UserId),
                NoiDung = commentViewModel.NoiDung,
                UserName = commentViewModel.UserName
            };
            _newsService.AddComment(comment);
            return Ok();
        }
    }
}