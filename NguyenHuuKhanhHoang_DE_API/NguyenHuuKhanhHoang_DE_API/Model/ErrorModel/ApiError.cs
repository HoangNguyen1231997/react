﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.ErrorModel
{
    public class ApiError
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public List<ValidationError> Errors { get; set; }
        public ApiError(ApiException apiException)
        {
            Message = apiException.Message;
            Errors = apiException.Errors;
        }
    }
}
