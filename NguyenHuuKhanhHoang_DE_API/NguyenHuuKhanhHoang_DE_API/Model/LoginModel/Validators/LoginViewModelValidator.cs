﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.LoginModel.Validators
{
    public class LoginViewModelValidator : AbstractValidator<LoginModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Tên đăng nhập yêu cầu nhập");
            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Mật khẩu yêu cầu nhập");
          
        }
    }
}
