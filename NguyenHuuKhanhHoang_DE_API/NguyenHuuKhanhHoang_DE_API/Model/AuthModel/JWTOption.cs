﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.AuthModel
{
    public class JWTOption
    {
        public string Audience { get; set; }
        public string SecretKey { get; set; }
    }
}
