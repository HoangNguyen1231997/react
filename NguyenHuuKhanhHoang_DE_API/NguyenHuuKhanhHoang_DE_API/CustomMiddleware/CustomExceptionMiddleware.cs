﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NguyenHuuKhanhHoang_DE_API.Logging;
using NguyenHuuKhanhHoang_DE_API.Model.ErrorModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.CustomMiddleware
{
    public class CustomExceptionMiddleware
    {
        private const string JsonContentType = "application/json";
        private readonly RequestDelegate _next;
        ILoggerManager _logger;

        public CustomExceptionMiddleware(RequestDelegate next, ILoggerManager logger)
        {
            _logger = logger;
              _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
           // context.Response.ContentType = "application/json";
           
            if(exception is ApiException ex)
            {
                context.Response.StatusCode = 500;
                string jsonString = JsonConvert.SerializeObject(new ApiError(ex));
                return context.Response.WriteAsync(jsonString);
            }
            else
            {
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    string jsonString = JsonConvert.SerializeObject("Unauthorize");
                    return context.Response.WriteAsync(jsonString);
                
            }
        }

    }
}
