﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.JWTToken
{
   public interface IJwtTokenHandler
    {
        string WriteToken(JwtSecurityToken jwt);
    }
}
