﻿using Microsoft.Extensions.Options;
using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.JWTToken
{
    public class JwtFactory :  IJwtFactory
    {
        private readonly IJwtTokenHandler _jwtTokenHandler;
        private readonly JwtIssuerOptions _jwtOptions;

        public JwtFactory(IJwtTokenHandler jwtTokenHandler, IOptions<JwtIssuerOptions> jwtOptions)
        {
            _jwtTokenHandler = jwtTokenHandler;
            _jwtOptions = jwtOptions.Value;
        }

        public AccessToken GenerateTokenJWT(long id ,string userName, string Role)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name , userName),
                new Claim(ClaimTypes.Role , Role),
            };

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                null,
                null,
                claims,
                _jwtOptions.NotBefore,
                _jwtOptions.Expiration,
                _jwtOptions.SigningCredentials);

            return new AccessToken(id, _jwtTokenHandler.WriteToken(jwt), (int)_jwtOptions.ValidFor.TotalSeconds);
        }
    }
}
