import React, {Component} from 'react';
class Footer extends Component {
    render(){
        return (
            <footer className="footer">
            <div className="container">
              <div className="row">
                <div className="col-lg-7">
                  <div className="widget">
                    <div className="footer-text text-left">
                      <div className="social">
                        <a  href="https://reactjs.org"  data-toggle="tooltip" data-placement="bottom" title="Facebook">
                          <i className="fa fa-facebook" />
                        </a>
                        <a  href="https://reactjs.org" data-toggle="tooltip" data-placement="bottom" title="Twitter">
                          <i className="fa fa-twitter" />
                        </a>
                        <a  href="https://reactjs.org"  data-toggle="tooltip" data-placement="bottom" title="Instagram">
                          <i className="fa fa-instagram" />
                        </a>
                        <a  href="https://reactjs.org" data-toggle="tooltip" data-placement="bottom" title="Google Plus">
                          <i className="fa fa-google-plus" />
                        </a>
                        <a   href="https://reactjs.org" data-toggle="tooltip" data-placement="bottom" title="Pinterest">
                          <i className="fa fa-pinterest" />
                        </a>
                      </div>
                      <hr className="invis" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 text-center">
                  <br />
                  <div className="copyright">© NashTech. Design: Khanh Hoang.</div>
                </div>
              </div>
            </div>
          </footer>
           );
    }
 
}

export default Footer;
