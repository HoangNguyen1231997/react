import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import ApiService from '../Service/ApiService';
import { Link} from "react-router-dom";
class HomeBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      news: [],
      newsPagin: [],

    };
  }
  componentDidMount() {
    ApiService("Home/GetBodyNews").then(res => {
      this.setState({
        news: res.data
      })
      this.setState({
        newsPagin: this.state.news.filter((u, i) => i < 4)
      })
    })
  }
  handlePageChange(pageNumber) {
    var skipValue = (4 * pageNumber) - 4
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
    this.setState({
      newsPagin: this.state.news.filter((u, i) => i >=skipValue).filter((u, i) => i < 4)
    })
  }
  render() {
    var news1 = this.state.newsPagin
    var element = news1.map((news, index) => {
      return <div key={index}>
        <div  className="blog-box row">
          <div className="col-md-4">
            <div className="post-media">
            
              <span href="tech-single.html" >
                <img src={news.newsDetail.imageUrl} alt="" className="img-fluid" />
                <div className="hovereffect" />
              </span>
            </div>
          </div>
          <div className="blog-meta big-meta col-md-8">
            <h4><Link to={"/chi-tiet/"+news.id}>{news.tieuDe}</Link></h4>
            <p>{news.newsDetail.noiDungSub}</p>
            <small className="firstsmall"><Link className="bg-orange" to={"/chi-tiet/"+news.id}>{news.theLoai}</Link></small>
            <small><Link to={"/chi-tiet/"+news.id}>{news.newsDetail.thoiGianDang}</Link></small>
            <small><Link to={"/chi-tiet/"+news.id}>by {news.user.name}</Link></small>
            <small><span  ><i className="fa fa-eye" /> 1114</span></small>
          </div>
        </div>
        <hr className="invis" />
      </div>
    });
    return (
      <div>
        {element}
        <div style={{ paddingLeft: '400px' }}>
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={4}
            totalItemsCount={this.state.news.length}
            pageRangeDisplayed={3}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange.bind(this)}
          />
        </div>
      </div>
    );
  }

}

export default HomeBody;
