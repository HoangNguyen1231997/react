import React, { Component } from 'react';
import ApiService from '../Service/ApiService';
import {Link } from "react-router-dom";
class HomeTop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: []
        }
    }
    componentDidMount() {
        ApiService("Home/GetHeaderNews").then(res => {
                this.setState({
                    news: res.data
                })
        }).catch(err => {
            console.log("Loi", err)
        })
    }
    render() {
        var news = this.state.news
        var element = news.map((news, index) => {
            return <div key={index} className={"slot-" + index}>
                <div className="masonry-box post-media">
                    <img src={news.newsDetail.imageUrl} alt="" className="img-fluid" />
                    <div className="shadoweffect">
                        <div className="shadow-desc">
                            <div className="blog-meta">
                                <span className="bg-orange"><Link to={"/chi-tiet/"+news.id}>{news.theLoai}</Link></span>
                                
                                <h4><Link to={"/chi-tiet/"+news.id}>{news.tieuDe}</Link></h4>
                                <small><Link to={"/chi-tiet/"+news.id}>{news.newsDetail.thoiGianDang}</Link></small>
                                <small><Link to={"/chi-tiet/"+news.id}>by {news.user.name}</Link></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        });
        return (
            <div>
                {element}
            </div>
        );
    }

}

export default HomeTop;
