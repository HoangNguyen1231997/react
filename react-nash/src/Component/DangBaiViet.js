import React, { Component } from 'react';
import Header from '../Component/Header';
import UploadFile from '../Service/UploadFile';
import ApiService from '../Service/ApiService';
import { toast, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Redirect } from "react-router";
import { NavLink } from "react-router-dom";
toast.configure()
class DangBaiViet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TieuDe:"",
            UserId:"",
            TheLoai:"",
            NoiDung:"",
            NoiDungSub:"",
            ImageUrl:"",
            ImageUrlSub:"",
            TieuDeValid:"",
            TheLoaiValid:"",
            NoiDungValid:"",
            NoiDungSubValid:"",
            isSuccess:false,
            isAuthen:true,
            isLogin:false,
            userName:""
        }
        this.onFileChinhSelected = this.onFileChinhSelected.bind(this)
        this.onFilePhuSelected = this.onFilePhuSelected.bind(this)
        this.onChange=this.onChange.bind(this)
        this.Send=this.Send.bind(this)
    }
    componentDidMount() {
        const asyncLocalStorageData = {
          getItem: async function (key) {
            await null;
            return localStorage.getItem(key)
              ;
          }
        };
        asyncLocalStorageData.getItem('AccessUserKey').then((value) => {
          if (value != null) {
            this.setState({
              userName: JSON.parse(value).userName,
              UserId: JSON.parse(value).id
            });
            ApiService("Home/CheckPermission").then(res => {
              this.setState({
                isLogin: true,
                isAuthen:true
              })
            }).catch((error) => {
                toast.error("Bạn không có quyền thực hiện chức năng này", { position: ToastPosition.BOTTOM_RIGHT });
              this.setState({
                isLogin: false,
                isAuthen:false
              })
            
            });
          }
    
        });
    
    
      }
    Send(){
        if(this.state.ImageUrl===""){
            toast.error("Vui lòng up ảnh chính", { position: ToastPosition.BOTTOM_RIGHT });
        }
        if(this.state.ImageUrlSub===""){
            toast.error("Vui lòng up ảnh phụ", { position: ToastPosition.BOTTOM_RIGHT });
        }
        if(this.state.ImageUrlSub!=="" && this.state.ImageUrl!==""){
            this.setState({
                TieuDeValid:"",
                TheLoaiValid:"",
                NoiDungValid:"",
                NoiDungSubValid:""
            })
            ApiService("Home/DangBaiViet", "POST", this.state).then(res => {
                this.setState({
                    isSuccess: true
                })
              
                toast.success("Đăng bài thành công", { position: ToastPosition.BOTTOM_RIGHT });
                
            }).catch((error) => {
                if (error.response.data.Errors != null) {
                    error.response.data.Errors.forEach((err, index) => {
                        this.setState({
                            [err.Field + "Valid"]: err.Message
                        });
                    })
                    toast.error("Đăng bài thất bại", { position: ToastPosition.BOTTOM_RIGHT });
                } else {
                    toast.error("Phiên làm việc của bạn đã hết vui lòng đăng nhập lại", { position: ToastPosition.BOTTOM_RIGHT });
                    this.setState({
                      isLogin: false,
                      isAuthen:false
                    })
                }
            });
    
        }
    }
    onFileChinhSelected(event){
        UploadFile(event.target.files[0]).then(result =>{
            this.setState({
                ImageUrl:result.data.value
            })
           
        })
        
    }
    onChange(event) {
        
                var name = event.target.name;
                var value = event.target.value
                this.setState({
                    [name]: value
                });
            }
    onFilePhuSelected(event){
        UploadFile(event.target.files[0]).then(result =>{
            this.setState({
                ImageUrlSub:result.data.value
            })
           
        })
        
    }
    render() {
        if (this.state.isAuthen === false) {
            return <Redirect to={{ pathname: "/home" }} />;
         }
        if (this.state.isSuccess === true) {
            return <Redirect to={{ pathname: "/home" }} />;
         }
        return (
            <div>
                <Header />
                <div className="page-title lb single-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <h2>
                                    <i className="fa fas fa-share bg-orange" /> Đăng bài mới
                                    <small className="hidden-xs-down hidden-sm-down">Tài khoản:{this.state.userName}</small>
                                </h2>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12 hidden-xs-down hidden-sm-down">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">
                                    <NavLink to="/home" >Home</NavLink>
                                    </li>
                                    <li className="breadcrumb-item active">Đăng bài viết</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="section wb">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="page-wrapper">
                                    <div className="row">
                                        <div className="col-lg-7">
                                            <div className="form-wrapper">
                                                <input type="text" className="form-control" placeholder="Tiêu đề bài viết" 
                                                 name="TieuDe" value={this.state.TieuDe}
                                                 onChange={this.onChange}/>
                                                <span style={{ color: 'red' }}>{this.state.TieuDeValid}</span>
                                                <textarea className="form-control" placeholder="Nội dung" 
                                                 name="NoiDung" value={this.state.NoiDung}
                                                 onChange={this.onChange} />
                                                <span style={{ color: 'red' }} >{this.state.NoiDungValid}</span>
                                                <textarea className="form-control" placeholder="Nội dung phụ" 
                                                name="NoiDungSub" value={this.state.NoiDungSub}
                                                onChange={this.onChange} />
                                                <span style={{ color: 'red' }} >{this.state.NoiDungSubValid}</span>
                                                <input type="text" className="form-control" placeholder="Thể loại" 
                                                name="TheLoai" value={this.state.TheLoai}
                                                onChange={this.onChange}/>
                                                <span style={{ color: 'red' }}>{this.state.TheLoaiValid}</span>
                                                <div className="form-group mt-3">
                                                    <label className="mr-2">Upload ảnh chính:</label>
                                                    <input type="file" onChange = {this.onFileChinhSelected}/>
                                                </div>
                                                <span style={{ color: 'red' }} />
                                                <br />
                                                <div className="form-group mt-3">
                                                    <label className="mr-2">Upload ảnh phụ:</label>
                                                    <input type="file" onChange = {this.onFilePhuSelected} />
                                                </div>
                                                <span style={{ color: 'red' }} />
                                                <br />
                                                <button className="btn btn-primary" onClick={this.Send}>Đăng
                                                    <i className="fa fas fa-share" />
                                                </button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

}

export default DangBaiViet;
