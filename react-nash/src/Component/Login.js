import React, { Component } from 'react';
import ApiService from '../Service/ApiService';
import { toast, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { setAuth } from '../Service/AuthService';
import { Redirect } from "react-router";
import Header from '../Component/Header';
import { NavLink } from "react-router-dom";
toast.configure()
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            UserName: "",
            Password: "",
            UserNameValid: "",
            PasswordValid: "",
            token: [],
            isLogin: false,
        }
        this.onChange = this.onChange.bind(this);
        this.Send = this.Send.bind(this);
        this.setAuth = this.setAuth.bind(this);

    }
    onChange(event) {

        var name = event.target.name;
        var value = event.target.value
        this.setState({
            [name]: value
        });
    }
    setAuth() {

        setAuth(this.state.token);
    }
    Send() {
        this.setState({
            PasswordValid: "",
            UserNameValid: ""
        })
        ApiService("Login/Login", "POST", this.state).then(res => {
            this.setState({
                token: res.data,
                isLogin: true
            })
            this.setAuth();
            toast.success("Đăng nhập thành công", { position: ToastPosition.BOTTOM_RIGHT });
            
        }).catch((error) => {
            if (error.response.data.Errors != null) {
                error.response.data.Errors.forEach((err, index) => {
                    this.setState({
                        [err.Field + "Valid"]: err.Message
                    });
                })
                toast.error("Đăng nhập thất bại", { position: ToastPosition.BOTTOM_RIGHT });
            } else {
                toast.error(error.response.data.Message, { position: ToastPosition.BOTTOM_RIGHT })
            }
        });

    }
    render() {
        if (this.state.isLogin === true) {
                return <Redirect to={{ pathname: "/home" }} />;
        }
        return (
            <div>
                <Header />
                <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css" />
                <div className="container" style={{ paddingTop: '80px' }}>
                    <div className="row">
                        <div className="span12">
                            <div className="form-horizontal">
                                <fieldset>
                                    <div id="legend">
                                        <legend >Đăng nhập</legend>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Tên đăng nhập</label>
                                        <div className="controls">
                                            <input type="text" className="input-xlarge"
                                                name="UserName" value={this.state.UserName}
                                                onChange={this.onChange} />
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }}>{this.state.UserNameValid}</span>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Mật khẩu</label>
                                        <div className="controls">
                                            <input type="password" className="input-xlarge"
                                                name="Password" value={this.state.Password}
                                                onChange={this.onChange} />
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }}>{this.state.PasswordValid}</span>
                                    </div>
                                    <NavLink to="/register" style={{ paddingLeft: '400px' }} className="nav-link">Đăng ký</NavLink>
                                    
                                    <div className="control-group">
                                        <div className="controls">
                                            <button className="btn btn-success" onClick={this.Send}>Login</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}

export default Login;






