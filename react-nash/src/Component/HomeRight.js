import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
class HomeRight extends Component {
  render() {
    return (
      <div className="sidebar">
        <div className="widget">
          <div className="banner-spot clearfix">
            <div className="banner-img">
              <img src="https://cdn.itviec.com/employers/nashtech/logo/social/nBaSjKy2e68gEwoJxsLk5zCe/logo.jpg" alt="" className="img-fluid" />
            </div>
          </div>
        </div>
        <div className="widget">
          <h2 className="widget-title">Follow Us</h2>
          <div className="row text-center">
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <NavLink to="/home" className="social-button facebook-button">
                <i className="fa fa-facebook" />
                <p>27k</p>
              </NavLink>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <NavLink to="/home" className="social-button twitter-button">
                <i className="fa fa-twitter" />
                <p>98k</p>
              </NavLink>

            </div>
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <NavLink to="/home" className="social-button google-button">
                <i className="fa fa-google-plus" />
                <p>17k</p>
              </NavLink>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <NavLink to="/home" className="social-button youtube-button">
                <i className="fa fa-youtube" />
                <p>22k</p>
              </NavLink>
            </div>
          </div>
        </div>
        <div className="widget">
          <div className="banner-spot clearfix">
            <div className="banner-img">
              <img src="https://farm5.staticflickr.com/4840/31051092187_0029c61a99_c.jpg" alt="" className="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default HomeRight;
