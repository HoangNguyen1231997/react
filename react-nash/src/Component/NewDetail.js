import React, { Component } from 'react';
import Header from '../Component/Header';
import HomeRight from '../Component/HomeRight';
import ApiService from '../Service/ApiService';
import { toast, ToastPosition } from 'react-toastify';
import { Redirect } from "react-router";
import { NavLink } from "react-router-dom";
import { Comments } from "../Model/Model"
toast.configure()
class NewDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: [],
            newsDetail: [],
            user: [],
            isAuthor: true,
            comment: [],
            binhLuanUser: "",
            userBinhLuanId: "",
            toaserMessager:false
        }
        this.binhLuan = this.binhLuan.bind(this);
        this.onChange = this.onChange.bind(this);
        this.getData = this.getData.bind(this);
    }
    getData(){
        let url = "Home/GetDetailNews?id=" + this.props.match.params.id
        ApiService(url, "GET").then(res => {
            this.setState({
                news: res.data,
                newsDetail: res.data.newsDetail,
                user: res.data.user,
                isAuthor: true,
                comment: res.data.comments
            })
        }).catch((error) => {
            this.setState({
                isAuthor: false,
                toaserMessager:true
            })
           
        });
    }
    componentDidMount() {
      this.getData()
    }
    componentDidUpdate(){
       
        if(this.state.toaserMessager===true){
            toast.error("Vui lòng đăng nhập trước khi thực hiện chức năng", { position: ToastPosition.BOTTOM_RIGHT })
        }else{
            this.getData();
        }
       
    }
    binhLuan() {
        if (this.state.binhLuanUser === "") {
            toast.error("Vui lòng nhập nội dung trước khi bình luận", { position: ToastPosition.BOTTOM_RIGHT })
        } else {
            const asyncLocalStorageData = {
                getItem: async function (key) {
                    await null;
                    return localStorage.getItem(key);
                }
            };
            asyncLocalStorageData.getItem('AccessUserKey').then((value) => {
                if (value != null) {
                    var comment = new Comments(this.state.binhLuanUser, this.state.news.id, value.id, JSON.parse(value).userName);

                    ApiService("Home/Comment", "POST", comment).then(res => {
                        toast.success("Bình luận thành công", { position: ToastPosition.BOTTOM_RIGHT })
                       this.setState({
                           binhLuanUser :""
                       })
                    }).catch((error) => {
                        this.setState({
                            isAuthor: false
                        });
                        toast.error("Phiên làm việc kết thúc vui lòng đăng nhập lại", { position: ToastPosition.BOTTOM_RIGHT })
                    });
                }

            });

        }
    }
    onChange(event) {

        var name = event.target.name;
        var value = event.target.value
        this.setState({
            [name]: value
        });
    }
    render() {
        if (this.state.isAuthor === false) {
            return <Redirect to={{ pathname: "/login" }} />;
        }
        var comment = <div></div>
        if (this.state.comment.length > 0) {
            comment = this.state.comment.map((comment, index) => {
                return <div key={index}>
                    <div className="media">
                        <span className="media-left">
                            <img src="https://cdn.itviec.com/employers/nashtech/logo/social/nBaSjKy2e68gEwoJxsLk5zCe/logo.jpg" alt="" className="rounded-circle" />
                        </span>
                        <div className="media-body">
                            <h4 className="media-heading user_name">{comment.userName}</h4>
                            <p>{comment.noiDung}</p>
                        </div>
                    </div>
                </div>

            })
        }
        return (
            <div>
                <Header />
                <section className="section single-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                <div className="page-wrapper">
                                    <div className="blog-title-area text-center">
                                        <ol className="breadcrumb hidden-xs-down">
                                            <li className="breadcrumb-item">
                                                <NavLink to="/home" >Home</NavLink>
                                            </li>
                                            <li className="breadcrumb-item active">{this.state.news.tieuDe}</li>
                                        </ol>
                                        <span className="color-orange"><span style={{ backgroundColor: 'orange', width: '176px', marginLeft: '330px' }} >{this.state.news.theLoai}</span></span>
                                        <h3>{this.state.news.tieuDe}</h3>
                                        <div className="blog-meta big-meta">
                                            <small>{this.state.newsDetail.thoiGianDang}</small>
                                            <small>by {this.state.user.name}</small>
                                            <small><i className="fa fa-eye" /> 2344</small>
                                        </div>
                                        <div className="post-sharing">
                                            <ul className="list-inline">
                                                <li> <NavLink to="/home" className="fb-button btn btn-primary" ><i className="fa fa-facebook" /> <span className="down-mobile">Share on Facebook</span></NavLink></li>
                                                <li><NavLink to="/home" className="tw-button btn btn-primary" ><i className="fa fa-twitter" /> <span className="down-mobile">Tweet on Twitter</span></NavLink></li>
                                                <li><NavLink to="/home" className="gp-button btn btn-primary" ><i className="fa fa-google-plus" /></NavLink></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="single-post-media">
                                        <img src={this.state.newsDetail.imageUrl} alt="" className="img-fluid" />
                                    </div>
                                    <div className="blog-content">
                                        <div className="pp">
                                            <p>{this.state.newsDetail.noiDung}</p>
                                        </div>
                                        <img src={this.state.newsDetail.imageUrlSub} alt="" className="img-fluid img-fullwidth" />
                                        <div className="pp">
                                            <p>{this.state.newsDetail.noiDungSub}</p>
                                        </div>
                                    </div>
                                    <div className="blog-title-area">
                                        <div className="tag-cloud-single">
                                            <span>Tags</span>
                                            <small>{this.state.news.theLoai}</small>

                                        </div>
                                        <div className="post-sharing">
                                            <ul className="list-inline">
                                                <li> <NavLink to="/home" className="fb-button btn btn-primary" ><i className="fa fa-facebook" /> <span className="down-mobile">Share on Facebook</span></NavLink></li>
                                                <li><NavLink to="/home" className="tw-button btn btn-primary" ><i className="fa fa-twitter" /> <span className="down-mobile">Tweet on Twitter</span></NavLink></li>
                                                <li><NavLink to="/home" className="gp-button btn btn-primary" ><i className="fa fa-google-plus" /></NavLink></li>
                                            </ul>
                                        </div>
                                    </div>


                                    <hr className="invis1" />
                                    <div className="custombox authorbox clearfix">
                                        <h4 className="small-title">Người đăng bài</h4>
                                        <div className="row">
                                            <div className="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <img src="https://cdn.itviec.com/employers/nashtech/logo/social/nBaSjKy2e68gEwoJxsLk5zCe/logo.jpg" alt="" className="img-fluid rounded-circle" />
                                            </div>
                                            <div className="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                                <h4><span >{this.state.user.name}</span></h4>
                                                <p>{this.state.user.gioiThieu}</p>
                                                <div className="topsocial">
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Facebook"><i className="fa fa-facebook" /></a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Youtube"><i className="fa fa-youtube" /></a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i className="fa fa-pinterest" /></a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Twitter"><i className="fa fa-twitter" /></a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Instagram"><i className="fa fa-instagram" /></a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Website"><i className="fa fa-link" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr className="invis1" />
                                    <div className="custombox clearfix">
                                        <h4 className="small-title">{this.state.comment.length} bình luận</h4>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="comments-list">
                                                    {comment}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="invis1" />
                                    <div className="custombox clearfix">
                                        <h4 className="small-title">Bình luận</h4>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-wrapper">
                                                    <textarea className="form-control" placeholder="Your comment"
                                                        name="binhLuanUser" value={this.state.binhLuanUser}
                                                        onChange={this.onChange} />
                                                    <button className="btn btn-primary" onClick={this.binhLuan}>Bình luận</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12" style={{ paddingTop: '310px' }}>
                                <HomeRight />
                            </div>
                        </div>
                    </div></section>
            </div>
        );
    }

}

export default NewDetail;




