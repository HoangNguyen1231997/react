import React, { Component } from 'react';
import Header from '../Component/Header';
import { Redirect } from "react-router";
import { toast, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ApiService from '../Service/ApiService';
toast.configure()
class Register extends Component {
    constructor(props){
        super(props);
        this.state={
            UserName:"",
            Password:"",
            ConfirmPassword:"",
            GioiThieu:"",
            UserNameValid:"",
            PasswordValid:"",
            ConfirmPasswordValid:"",
            GioiThieuValid:"",
            isSuccessRegister:false
        }
        this.Send=this.Send.bind(this)
        this.onChange = this.onChange.bind(this)
    }
    Send() {
        this.setState({
            PasswordValid: "",
            UserNameValid: "",
            ConfirmPasswordValid:"",
            GioiThieuValid:""
        })
        ApiService("Login/Register", "POST", this.state).then(res => {
            this.setState({
                isSuccessRegister: true
            })
           
            toast.success("Đăng ký thành công", { position: ToastPosition.BOTTOM_RIGHT });
            
        }).catch((error) => {
            if (error.response.data.Errors != null) {
                error.response.data.Errors.forEach((err, index) => {
                    this.setState({
                        [err.Field + "Valid"]: err.Message
                    });
                })
                toast.error("Đăng ký thất bại", { position: ToastPosition.BOTTOM_RIGHT });
            } else {
                toast.error(error.response.data.Message, { position: ToastPosition.BOTTOM_RIGHT })
            }
        });

    }
    onChange(event) {
        
                var name = event.target.name;
                var value = event.target.value
                this.setState({
                    [name]: value
                });
            }
    render() {
        if (this.state.isSuccessRegister === true) {
            return <Redirect to={{ pathname: "/login" }} />;
        }
        return (
            <div>
                <Header />
                <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css" />
                <div className="container" style={{ paddingTop: '80px' }}>
                    <div className="row">
                        <div className="span12">
                            <div className="form-horizontal">
                                <fieldset>
                                    <div id="legend">
                                        <legend >Đăng ký</legend>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Tên đăng nhập</label>
                                        <div className="controls">
                                            <input type="text" className="input-xlarge" 
                                            name="UserName" value={this.state.UserName}
                                            onChange={this.onChange}/>
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }}>{this.state.UserNameValid}</span>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Mật khẩu</label>
                                        <div className="controls">
                                            <input type="password" className="input-xlarge"
                                            name="Password" value={this.state.Password}
                                            onChange={this.onChange} />
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }} >{this.state.PasswordValid}</span>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Xác nhận mật khẩu</label>
                                        <div className="controls">
                                            <input type="password" className="input-xlarge"
                                             name="ConfirmPassword" value={this.state.ConfirmPassword}
                                             onChange={this.onChange} />
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }} >{this.state.ConfirmPasswordValid}</span>
                                    </div>
                                    <div className="control-group">
                                        <label className="control-label">Giới thiệu</label>
                                        <div className="controls">
                                            <textarea className="input-xlarge" 
                                             name="GioiThieu" value={this.state.GioiThieu}
                                             onChange={this.onChange} />
                                        </div>
                                        <span style={{ paddingLeft: '25px', color: 'red' }} >{this.state.GioiThieuValid}</span>
                                    </div>
                                    <div className="control-group">
                                        <div className="controls">
                                            <button className="btn btn-success" onClick={this.Send}>Đăng ký</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Register;

