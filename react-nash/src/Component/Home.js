import React, { Component } from 'react';
import HomeRight from './HomeRight';
import HomeTop from './HomeTop';
import HomeBody from './HomeBody';
import Header from '../Component/Header';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      news: []
    }
  }
  render() {
    return (
      <div>
        <Header />
        <section className="section first-section">
          <div className="container-fluid">
            <div className="masonry-blog clearfix">
              <HomeTop />
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div className="page-wrapper">
                  <div className="blog-top clearfix">
                    <h4 className="pull-left">Recent News <span ><i className="fa fa-rss" /></span></h4>
                  </div>
                  <div className="blog-list clearfix">
                   <HomeBody />
                  </div>
                </div>
                
              </div>
              <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <HomeRight />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }

}

export default Home;
