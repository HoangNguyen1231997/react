import React, { Component } from 'react';
import ApiService from '../Service/ApiService';
import Header from '../Component/Header';
import { Redirect } from "react-router";
import { toast, ToastPosition } from 'react-toastify';
import UploadFile from '../Service/UploadFile';
import { NavLink } from "react-router-dom";
toast.configure()
class SuaBaiViet extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            Id: "",
            TieuDe: "",
            UserId: "",
            TheLoai: "",
            NoiDung: "",
            NoiDungSub: "",
            ImageUrl: "",
            ImageUrlSub: "",
            TieuDeValid: "",
            TheLoaiValid: "",
            Type: "",
            NewDetailId: "",
            NoiDungValid: "",
            NoiDungSubValid: "",
            isSuccess: false,
            isAuthen: true,
            userName: "",
            updateImage: false,
            updateImagePhu: false,
        }

        this.onChange = this.onChange.bind(this);
        this.getData = this.getData.bind(this);
        this.UpdateAnhChinh = this.UpdateAnhChinh.bind(this);
        this.UpdateAnhPhu = this.UpdateAnhPhu.bind(this);
        this.onFileChinhSelected = this.onFileChinhSelected.bind(this)
        this.onFilePhuSelected = this.onFilePhuSelected.bind(this)
        this.UpdateNews = this.UpdateNews.bind(this)
    }
    onFileChinhSelected(event) {
        UploadFile(event.target.files[0]).then(result => {
            this.setState({
                ImageUrl: result.data.value
            })

        })

    }
    onFilePhuSelected(event) {
        UploadFile(event.target.files[0]).then(result => {
            this.setState({
                ImageUrlSub: result.data.value
            })

        })

    }
    UpdateAnhChinh() {
        this.setState({
            updateImage: true
        })
    }
    UpdateAnhPhu() {
        this.setState({
            updateImagePhu: true
        })
    }
    UpdateNews() {
        this.setState({
            TieuDeValid: "",
            TheLoaiValid: "",
            NoiDungValid: "",
            NoiDungSubValid: "",
        })
        
        ApiService("Home/UpdateBaiViet", "POST", this.state).then(res => {
            this.setState({
                isSuccess: true
            })
            toast.success("Sửa bài thành công", { position: ToastPosition.BOTTOM_RIGHT });
        }).catch((error) => {
            if (error.response.data.Errors != null) {
                error.response.data.Errors.forEach((err, index) => {
                    this.setState({
                        [err.Field + "Valid"]: err.Message
                    });
                })
               
                toast.error("Sửa bài thất bại", { position: ToastPosition.BOTTOM_RIGHT });
            } else {
                toast.error("Phiên làm việc của bạn đã hết vui lòng đăng nhập lại", { position: ToastPosition.BOTTOM_RIGHT });
                this.setState({
                    isAuthen: false
                })
            }
        });

    }
    getData() {
        const asyncLocalStorageData = {
            getItem: async function (key) {
                await null;
                return localStorage.getItem(key)
                    ;
            }
        };
        asyncLocalStorageData.getItem('AccessUserKey').then((value) => {
            if (value != null) {
                this.setState({
                    userName: JSON.parse(value).userName
                });
            }
        });
        let url = "Home/GetDetailNews?id=" + this.props.match.params.id
        ApiService(url, "GET").then(res => {
            console.log(res)
            this.setState({
                Id: res.data.id,
                TieuDe: res.data.tieuDe,
                UserId: res.data.userId,
                TheLoai: res.data.theLoai,
                Type: res.data.type,
                NewDetailId: res.data.newsDetail.id,
                NoiDung: res.data.newsDetail.noiDung,
                NoiDungSub: res.data.newsDetail.noiDungSub,
                ImageUrl: res.data.newsDetail.imageUrl,
                ImageUrlSub: res.data.newsDetail.imageUrlSub
            })
        }).catch((error) => {
            this.setState({
                isAuthen: false,
            })
            toast.error("Vui lòng đăng nhập trước khi thực hiện chức năng", { position: ToastPosition.BOTTOM_RIGHT })
        });
    }
    componentDidMount() {
        this.getData()
    }
    onChange(event) {
        var name = event.target.name;
        var value = event.target.value
        this.setState({
            [name]: value
        });
    }
    render() {
        var imageChinh = <div></div>
        var imagePhu = <div></div>
        var buttonAnhChinh = <div></div>
        var buttonAnhPhu = <div></div>
        if (this.state.updateImage === true) {
            imageChinh = <div className="form-group mt-3">
                <label className="mr-2">Upload ảnh chính:</label>
                <input type="file" onChange={this.onFileChinhSelected} />
            </div>
        } else {
            buttonAnhChinh = <button type="button" id="buttonanhchinh" className="btn btn-primary" onClick={this.UpdateAnhChinh} style={{ marginLeft: '20px' }}>Sửa</button>
        }
        if (this.state.updateImagePhu === true) {
            imagePhu = <div className="form-group mt-3">
                <label className="mr-2">Upload ảnh phụ:</label>
                <input type="file" onChange={this.onFilePhuSelected} />
            </div>
        } else {
            buttonAnhPhu = <button type="button" className="btn btn-primary" style={{ marginLeft: '20px' }} onClick={this.UpdateAnhPhu}>Sửa</button>
        }
        if (this.state.isAuthen === false) {
            return <Redirect to={{ pathname: "/login" }} />;
        }
        if (this.state.isSuccess === true) {
            return <Redirect to={{ pathname: "/home" }} />;
        }
        return (
            <div>
                <Header />
                <div className="page-title lb single-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <h2>
                                    <i className="fa fas fa-share bg-orange" /> Chỉnh sửa bài viết
                  <small className="hidden-xs-down hidden-sm-down">Tài khoản: {this.state.userName}</small>
                                </h2>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12 hidden-xs-down hidden-sm-down">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">
                                        <NavLink to="/home" >Home</NavLink>
                                    </li>
                                    <li className="breadcrumb-item active">Chỉnh sửa bài viết</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="section wb">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="page-wrapper">
                                    <div className="row">
                                        <div className="col-lg-7">
                                            <div className="form-wrapper">
                                                <label className="mr-2"> Tiêu đề:</label>
                                                <span style={{ color: 'red' }} >{this.state.TieuDeValid}</span>
                                                <input type="text" className="form-control" placeholder="Tiêu đề bài viết"
                                                    name="TieuDe" value={this.state.TieuDe}
                                                    onChange={this.onChange} />
                                                <label className="mr-2"> Nội dung:</label>
                                                <span style={{ color: 'red' }} >{this.state.NoiDungValid}</span>
                                                <textarea className="form-control" placeholder="Nội dung"
                                                    name="NoiDung" value={this.state.NoiDung}
                                                    onChange={this.onChange} />
                                                <label className="mr-2"> Nội dung phụ:</label>
                                                <span style={{ color: 'red' }} >{this.state.NoiDungSubValid}</span>
                                                <textarea className="form-control" placeholder="Nội dung phụ"
                                                    name="NoiDungSub" value={this.state.NoiDungSub}
                                                    onChange={this.onChange} />
                                                <label className="mr-2"> Thể loại:</label>
                                                <span style={{ color: 'red' }}>{this.state.TheLoaiValid}</span>
                                                <input type="text" className="form-control" placeholder="Thể loại"
                                                    name="TheLoai" value={this.state.TheLoai}
                                                    onChange={this.onChange} />
                                                <div className="form-group mt-3">
                                                    <label className="mr-2"> Ảnh chính:</label>
                                                    <img style={{ width: '120px', height: '120px' }} alt="Anh chinh" src={this.state.ImageUrl} />
                                                    {buttonAnhChinh}
                                                    {imageChinh}
                                                    <span style={{ color: 'red' }} />
                                                    <br />
                                                </div>
                                                <div className="form-group mt-3">
                                                    <label className="mr-2"> Ảnh phụ:</label>
                                                    <img style={{ width: '120px', height: '120px', marginLeft: '16px' }} src={this.state.ImageUrlSub} alt="anh phu" />
                                                    {buttonAnhPhu}
                                                    {imagePhu}
                                                    <span style={{ color: 'red' }} />
                                                    <br />
                                                </div>
                                                <button className="btn btn-primary" onClick={this.UpdateNews}>Sửa
                          <i className="fa fas fa-share" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

}

export default SuaBaiViet;
