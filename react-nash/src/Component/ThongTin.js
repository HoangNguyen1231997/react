import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import ApiService from '../Service/ApiService';
import {Link } from "react-router-dom";
import { Redirect } from "react-router";
import { toast, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Header from '../Component/Header';
toast.configure()
class ThongTin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 1,
            news: [],
            users: [],
            newsPagin: [],
            userName: "",
            userId: "",
            isAuthen: true,
            newsId: ""
        };
        this.getDetail = this.getDetail.bind(this)
        this.DeleteNews = this.DeleteNews.bind(this)
    }
    getDetail() {
        const asyncLocalStorageData = {
            getItem: async function (key) {
                await null;
                return localStorage.getItem(key)
                    ;
            }
        };
        ApiService("Home/CheckToken").then(res => {
            this.setState({
                isAuthen: true
            })
            asyncLocalStorageData.getItem('AccessUserKey').then((value) => {
                if (value != null) {
                    this.setState({
                        userName: JSON.parse(value).userName
                    });

                    ApiService("Home/GetDetailUser?id=" + JSON.parse(value).id).then(res => {
                        this.setState({
                            news: res.data.news,
                            users: res.data,
                            newsId: res.data.news.id
                        })
                        this.setState({
                            newsPagin: res.data.news.filter((u, i) => i < 4)
                        })
                    })
                }

            });
        }).catch((error) => {
            console.log("loi", error)
            this.setState({
                isAuthen: false
            })
            toast.error("Phiên làm việc của bạn đã hết vui lòng đăng nhập lại", { position: ToastPosition.BOTTOM_RIGHT });
        });
    }
    componentDidMount() {

        this.getDetail()
    }
    handlePageChange(pageNumber) {
        var skipValue = (4 * pageNumber) - 4
        console.log(`active page is ${pageNumber}`);
        this.setState({ activePage: pageNumber });
        this.setState({
            newsPagin: this.state.news.filter((u, i) => i >= skipValue).filter((u, i) => i < 4)
        })
    }
    DeleteNews(id) {
        ApiService("Home/DeleteNew?id=" + id, "POST").then(res => {
            this.setState({
                activePage: 1
            })
            this.getDetail()
            toast.success("Xóa bài viết thành công", { position: ToastPosition.BOTTOM_RIGHT });
        }).catch((error) => {
            console.log("loi", error)
            this.setState({
                isAuthen: false
            })
            toast.error("Phiên làm việc của bạn đã hết vui lòng đăng nhập lại", { position: ToastPosition.BOTTOM_RIGHT });
        });
    }
    render() {
        if (this.state.isAuthen === false) {
            return <Redirect to={{ pathname: "/home" }} />;
        }
        var element = <div><h3> Bạn chưa có bài viết nào </h3></div>
        var news1 = this.state.newsPagin
        var pagin = <div></div>
        if (news1.length > 0) {
            pagin = <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={4}
                totalItemsCount={this.state.news.length}
                pageRangeDisplayed={3}
                itemClass="page-item"
                linkClass="page-link"
                onChange={this.handlePageChange.bind(this)}
            />
            element = news1.map((news, index) => {
                return <div key={index}>
                    <div className="blog-box row">
                        <div className="col-md-4">
                            <div className="post-media">

                                <span >
                                    <img src={news.newsDetail.imageUrl} alt="" className="img-fluid" />
                                    <div className="hovereffect" />
                                </span>
                            </div>
                        </div>
                        <div className="blog-meta big-meta col-md-8">

                            <h4><Link to={"/chi-tiet/" + news.id}>{news.tieuDe}</Link></h4>
                            <p>{news.newsDetail.noiDungSub}</p>
                            <Link className="bg-orange" to={"/sua-bai-viet/" + news.id}>Sửa</Link>
                            <button className="bg-orange" style={{ marginLeft: '10px', marginRight: '10px', height: '26px' }} onClick={() => this.DeleteNews(news.id)} >Xóa</button>
                            <small><Link to={"/chi-tiet/" + news.id}>{news.newsDetail.thoiGianDang}</Link></small>
                            <small><Link to={"/chi-tiet/" + news.id}>by {this.state.users.name}</Link></small>

                        </div>
                    </div>
                    <hr className="invis" />
                </div>
            });
        }

        return (
            <div>
                <Header />
                <div className="page-title lb single-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <h2>
                                    <i className="fa fa-star bg-orange" />Tài khoản: {this.state.userName}</h2>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12 hidden-xs-down hidden-sm-down">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">
                                        <Link to="/home" >Home</Link>
                                    </li>
                                    <li className="breadcrumb-item">
                                        <span >Tài khoản</span>
                                    </li>
                                    <li className="breadcrumb-item active">{this.state.users.name}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="section">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                <div className="page-wrapper">
                                    <div className="custombox authorbox clearfix">
                                        <h4 className="small-title">Thông tin</h4>
                                        <div className="row">
                                            <div className="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <img src="https://cdn.itviec.com/employers/nashtech/logo/social/nBaSjKy2e68gEwoJxsLk5zCe/logo.jpg" alt="" className="img-fluid rounded-circle" />
                                            </div>
                                            <div className="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                                <h4>
                                                    <Link to="/thong-tin" >{this.state.users.name}</Link>
                                                </h4>
                                                <p>{this.state.users.gioiThieu}</p>
                                                <div className="topsocial">
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Facebook">
                                                        <i className="fa fa-facebook" />
                                                    </a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Youtube">
                                                        <i className="fa fa-youtube" />
                                                    </a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Pinterest">
                                                        <i className="fa fa-pinterest" />
                                                    </a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Twitter">
                                                        <i className="fa fa-twitter" />
                                                    </a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Instagram">
                                                        <i className="fa fa-instagram" />
                                                    </a>
                                                    <a href="# " data-toggle="tooltip" data-placement="bottom" title="Website">
                                                        <i className="fa fa-link" />
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="invis1" />
                                    <h2> Bài viết đã đăng</h2>
                                    <div>
                                        <div>
                                            {element}
                                        </div>
                                        <div style={{ marginLeft: '250px' }}>
                                            {pagin}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

}

export default ThongTin;





