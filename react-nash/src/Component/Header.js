import React, { Component } from 'react';
import { NavLink,Link } from "react-router-dom";
import ApiService from '../Service/ApiService';
import { destroyAccessUser } from '../Service/AuthService'
import { toast, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      userName: ""
    }
    this.LogOut = this.LogOut.bind(this)
  }
  LogOut() {
    destroyAccessUser();
    this.setState({
      userName: "",
      isLogin: false
    })
    toast.success("Đăng xuất thành công", { position: ToastPosition.BOTTOM_RIGHT });
  }
  componentDidMount() {
    const asyncLocalStorageData = {
      getItem: async function (key) {
        await null;
        return localStorage.getItem(key)
          ;
      }
    };
    asyncLocalStorageData.getItem('AccessUserKey').then((value) => {
      if (value != null) {
        this.setState({
          userName: JSON.parse(value).userName
        });
        ApiService("Home/CheckToken").then(res => {
          this.setState({
            isLogin: true
          })
        }).catch((error) => {
          console.log("loi", error)
          this.setState({
            isLogin: false
          })
          destroyAccessUser();
          this.setState({
            userName: "",
            isLogin: false
          })
        });
      }

    });


  }
  render() {
    var dangbaiviet =<div></div>
    var element = <div>
      <li className="nav-item dropdown has-submenu  ">
      <NavLink to="/thong-tin" activeStyle={{
                    backgroundColor:'gray',
                    color:'red'
                  }} className="nav-link dropdown-toggle dropdown">{this.state.userName}</NavLink>
        <ul className="dropdown-menu " aria-labelledby="dropdown01">
          <li>
            <div>
              <div className="dropdown-content">
              <Link to="/thong-tin" >Thông tin tài khoản</Link>
                <Link to="/home" onClick={this.LogOut} >Đăng xuất</Link>

              </div>
            </div>
          </li>
        </ul>
      </li>

    </div>
    if (this.state.isLogin === false) {
      element = <li className="nav-item">
        <NavLink to="/login" activeStyle={{
                    backgroundColor:'gray',
                    color:'red'
                  }} className="nav-link">Đăng nhập</NavLink>
      </li>
    }
    if (this.state.isLogin === true) {
      dangbaiviet = <li >
                  <NavLink to="/dang-bai-viet" activeStyle={{
                    backgroundColor:'gray',
                    color:'red'
                  }} className="nav-link">Đăng bài viết</NavLink>
                </li>
    }
    return (
      <header className="tech-header header">
        <div className="container-fluid">
          <nav className="navbar navbar-toggleable-md  fixed-top bg-inverse">
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="/navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
            </button>
            <Link className="navbar-brand" to="/home" >
              <img src="https://www.nashtechglobal.com/wp-content/themes/nashtech/library/images/NashTech-logo-new.svg" style={{ height: '50px', width: '100px' }} alt="" />
            </Link>

            <div className="collapse navbar-collapse" id="navbarCollapse">
              <ul className="navbar-nav mr-auto">

                <li className="nav-item">
                  <NavLink to="/home" activeStyle={{
                    backgroundColor:'gray',
                    color:'red'
                  }} className="nav-link">Home</NavLink>
                </li>
                {dangbaiviet}
                {element}
                
              </ul>
              <ul className="navbar-nav mr-2">
                <li className="nav-item">
                  <Link to="/home" className="nav-link"> <i className="fa fa-rss" /></Link>

                </li>
                <li className="nav-item">
                  <Link to="/home" className="nav-link"> <i className="fa fa-android" /></Link>

                </li>
                <li className="nav-item">
                  <Link to="/home" className="nav-link"> <i className="fa fa-apple" /></Link>

                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
    );
  }

}

export default Header;




