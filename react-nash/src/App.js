import React from 'react';
import './App.css';

import Footer from './Component/Footer';
import RouterNash from './Router/RouterNash';
import { BrowserRouter as Router} from "react-router-dom";
function App() {
  return (
    <Router>
    <div>
      
      {/* <Home /> */}
      <RouterNash />
      <Footer />
    </div>
    </Router>
  );
}

export default App;
