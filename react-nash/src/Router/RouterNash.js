import React, { Component } from 'react';
import Home from '../Component/Home';
import Login from '../Component/Login';
import NewDetail from '../Component/NewDetail';
import Register from '../Component/Register';
import DangBaiViet from '../Component/DangBaiViet';
import SuaBaiViet from '../Component/SuaBaiViet';
import ThongTin from '../Component/ThongTin';
import { Switch, Route } from "react-router-dom";

class RouterNash extends Component {
    render() {
        return (

            <div>
                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/" exact={true}>
                        <Home />
                    </Route>
                    <Route path="/register" >
                        <Register />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/dang-bai-viet">
                        <DangBaiViet />
                    </Route>
                    <Route path="/sua-bai-viet/:id"render={(matchProps) =>
                        <SuaBaiViet
                            {...matchProps}
                        />
                    } />
                    <Route path="/thong-tin">
                        <ThongTin />
                    </Route>
                    <Route path='/chi-tiet/:id' render={(matchProps) =>
                        <NewDetail
                            {...matchProps}
                        />
                    } />
                    
                </Switch>



            </div>

        );
    }

}

export default RouterNash;
