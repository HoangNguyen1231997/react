import {setItem ,removeItem,getItem} from '../Service/LocalStorageHelper';
export  function setAuth(data) {
    setItem(data);
  }
  export  function getToken() {
    const accessUser = this.getAccessUser();
    if (accessUser != null) {
      return accessUser.accessToken;
    }
    return null;
    
  }
  export  function getAccessUser() {
    return getItem('AccessUserKey');
  }
  export  function destroyAccessUser() {
    removeItem('AccessUserKey');
  }