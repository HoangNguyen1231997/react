import axios from 'axios';
import { getAccessUser } from '../Service/AuthService';
export default function ApiService(url, method = 'GET', body) {
    let user = getAccessUser();
    let token = user!=null?user.accessToken.token:null;
    return axios({
        method: method,
        url: "https://localhost:5001/"+url,
        headers:{'Authorization': `Bearer ${token}`},
        data: body
        })
}