export  function setItem(data) {
    localStorage.setItem('AccessUserKey',JSON.stringify(data));
  }
 
  export  function removeItem(key) {
    localStorage.removeItem(key);
  }

  export  function getItem(key){
    if (localStorage.getItem('AccessUserKey')) {
      return JSON.parse(localStorage.getItem('AccessUserKey'));
    }
    return null;
  }

   