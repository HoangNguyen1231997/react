import axios from 'axios';
export default function UploadFile(file) {
    const url = 'https://localhost:5001/Home/UploadImage';
    const formData = new FormData();
    formData.append('Image', file, file.name);
    return axios.post(url,formData)
}